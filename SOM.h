/*
 * SOM.h
 */

#ifndef SOM_H_
#define SOM_H_

#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>

#include "Util.h"
#include "Neuron.h"
#include "define.h"

class SOM {
public:

	SOM(unsigned int mapWidth, unsigned int mapHeight, unsigned int vectorSize,
			unsigned int learningCycles, double radius, double learningRate);

	virtual ~SOM();

	bool Train(const std::vector<std::vector<double> > &data);

	void Test(const std::vector<std::vector<double> > &data, const std::vector<
			int> &labels);

	Neuron* FindWinningNeuron(const std::vector<double> &dataVector);

	void Save();



private:
	unsigned int GetWinningNeuronIndex(const std::vector<double> &dataVector);

	void Labeling(const std::vector<std::vector<double> > &data,
			const std::vector<int> &labels);

	void ConsoleOutput();

	std::vector<Neuron> mySOM;
	unsigned int myNumIterations; // nb iteration max
	double myIterationCount; // nb iteration actuel (double : calcul)

	unsigned int myCellWidth; // nb cell x
	unsigned int myCellHeight; // nb cell y

	Neuron* myWinningNeuron; //pointeur sur le neurone gagnant (pr chaque tour)

	double myNeighbourhoodRadius; //
	double myMapRadius; //

	double myTimeConstant; //
	double myLearningRate;
	double myInfluence;
};

#endif /* SOM_H_ */
