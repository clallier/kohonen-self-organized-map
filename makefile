#compilateur utilisé
CC = g++

#options de compilation et de linkage
CFLAGS = -W -Wall -Werror -ansi -pedantic -s -O6 -Os

#programme à créer
EXEC = Kohonen

#liste des fichiers à compiler
SRC = $(wildcard *.cpp)

#en faire une liste d'objet
OBJ = $(SRC:.cpp=.o)

#la création dépend du programme
all: $(EXEC)

#le programme dépend des fichiers objets
$(EXEC): $(OBJ)

#linkage
	@$(CC) -o $@ *.o $(LDFLAGS)

#compilation des fichiers objets nécessitant un cpp ET un h
%.o: %.cpp %.h
	@echo $<
	@$(CC) -o $@ -c $< $(CFLAGS)

#autres possibilités de construction
.PHONY: clean mrproper

clean:
	rm -rf *.o

mrproper: clean
	rm -rf $(EXEC)# creation de l'exe

