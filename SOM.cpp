/*
 * SOM.cpp
 */

#include "SOM.h"

using namespace std;

// ***************************************************************************
// ***************************************************************************
// ***************************************************************************
SOM::SOM(unsigned int mapWidth, unsigned int mapHeight,
		unsigned int vectorSize, unsigned int learningCycles, double radius,
		double learningRate) {

	myCellWidth = mapWidth;
	myCellHeight = mapHeight;
	myWinningNeuron = NULL;
	myIterationCount = 1;
	myNumIterations = learningCycles;
	myNeighbourhoodRadius = radius;
	myInfluence = 0;
	myLearningRate = learningRate;

	// creation de la carte de neurones
	for (double w = 0; w < mapWidth; w++) {
		for (double h = 0; h < mapHeight; h++) {
			mySOM.push_back(Neuron(w, h, vectorSize));
		}
	}

	// map radius initial
	myMapRadius = 10;

	//utiliser dans le calcul de voisinage de l'influence
	myTimeConstant = myNumIterations / (log(myMapRadius));
}

SOM::~SOM() {
}

// ***************************************************************************
// ***************************************************************************
// ***************************************************************************
bool SOM::Train(const vector<vector<double> > &data) {

	// verifie que les vecteurs ont la bonne taille
	if (data[0].size() != constSizeOfInputVector) {
		cout << "les vecteurs de la base ne font pas "
				<< constSizeOfInputVector << endl;
		return false;
	}

	// boucle d'apprentissage
	while (myNumIterations > 0) {

		std::cout << "cycles left: " << myNumIterations << std::endl;

		// choix d'un vecteur aleatoire dans la base
		int thisVector = Util::RandInt(0, (data.size() - 1));

		// calcul du neurone gangnant pour cet exemple
		myWinningNeuron = FindWinningNeuron(data[thisVector]);

		//calculde la ytaille du voisinage pour ce pas de temps
		myNeighbourhoodRadius = myMapRadius * exp((-myIterationCount
				/ myTimeConstant));

		// pour tous les neurones, ajuste ses poids en fonction de sa distance au neurone gagnant
		for (unsigned int n = 0; n < mySOM.size(); n++) {
			//calcul de la distance Euclidienne
			double distToWinner = (myWinningNeuron->X() - mySOM[n].X())
					* (myWinningNeuron->X() - mySOM[n].X())
					+ (myWinningNeuron->Y() - mySOM[n].Y())
							* (myWinningNeuron->Y() - mySOM[n].Y());
			/*
			 cout << "dist to  winner " << distToWinner <<endl;
			 cout << "winner x" << myWinningNeuron->X() << " y" << myWinningNeuron->Y() <<endl;
			 cout << "neuro x" << mySOM[n].X() << " y" << mySOM[n].Y() <<endl;
			 */
			double width = myNeighbourhoodRadius * myNeighbourhoodRadius;

			//si dans le voisinage : ajuste les poids
			if (distToWinner < width) {
				//calcul de l'influence : facteur d'ajustement des poids

				myInfluence = exp(-(distToWinner) / (2 * width));
				mySOM[n].AdjustWeights(data[thisVector], myLearningRate,
						myInfluence);
			}

		}// neurone suivant


		// ajuste le learningRate
		myLearningRate = constStartLearningRate * exp(-myIterationCount
				/ (double) myNumIterations);

		myIterationCount++;
		myNumIterations--;

	}
	Save();
	cout << "apprentissage OK " << endl;
	return true;
}

// ***************************************************************************
// ***************************************************************************
// ***************************************************************************
Neuron* SOM::FindWinningNeuron(const vector<double> &dataVector) {
	Neuron* winner = NULL;
	int n = GetWinningNeuronIndex(dataVector);
	winner = &mySOM[n];
	return winner;
}

// ***************************************************************************
// ***************************************************************************
// ***************************************************************************
unsigned int SOM::GetWinningNeuronIndex(const vector<double> &dataVector) {
	double distMin = 999999;
	unsigned int w = 0;
	for (unsigned int n = 0; n < mySOM.size(); n++) {
		double dist = mySOM[n].GetDistance(dataVector);
		if (dist < distMin) {
			distMin = dist;
			w = n;
		}
	}
	// cout << "Winner is "<< w <<endl;
	return w;
}

// ***************************************************************************
// ***************************************************************************
// ***************************************************************************
void SOM::Test(const std::vector<std::vector<double> > &data,
		const std::vector<int> &labels) {
	Labeling(data, labels);
	ConsoleOutput();

	double success = 0;
	for (unsigned int d = 0; d < data.size(); d++) {
		myWinningNeuron = FindWinningNeuron(data[d]);
		// classe du neurone gagnant == classe de l'item ?
		if (myWinningNeuron->Class() == labels[d]) {
			success++;
		}
	}
	cout << "success rate : " << (success / (double) data.size()*100.0) << endl;

}

// ***************************************************************************
// ***************************************************************************
// ***************************************************************************
void SOM::Labeling(const std::vector<std::vector<double> > &data,
		const std::vector<int> &labels) {

	vector<vector<int> > flags;

	// pour v valeurs d'indexes
	int maxVal = *max_element(labels.begin(), labels.end());
	vector<int> v(maxVal + 1);

	// pour n neurones
	for (unsigned int n = 0; n < mySOM.size(); n++) {
		flags.push_back(v);
	}

	// neurone gagnant pour chaq item
	unsigned int winningNeuronIndex = 0;
	for (unsigned int d = 0; d < data.size(); d++) {
		winningNeuronIndex = GetWinningNeuronIndex(data[d]);
		flags[winningNeuronIndex][labels[d]]++;
	}

	// pour chaq neurone, le plus haut score dans une classe
	// designe sa classe officielle
	for (unsigned int d = 0; d < flags.size(); d++) {
		int nClass = 0, nVal = 0;
		for (unsigned int n = 0; n < flags[d].size(); n++) {
			if (flags[d][n] > nVal) {
				nVal = flags[d][n];
				nClass = n;
			}
		}
		mySOM[d].SetClass(nClass);
	}
}

// ***************************************************************************
// ***************************************************************************
// ***************************************************************************
void SOM::ConsoleOutput() {
	int lastX = 0;
	for (unsigned int n = 0; n < mySOM.size(); n++) {

		if (mySOM[n].X() != lastX) {
			cout << endl;
			lastX = mySOM[n].X();
		}

		cout << mySOM[n].Class() << " ";
	}
	cout << endl;
}

// ***************************************************************************
// ***************************************************************************
// ***************************************************************************
void SOM::Save() {
}
