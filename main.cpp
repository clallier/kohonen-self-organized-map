/*
 * main.cpp
 */

#include "K.h"
#include "define.h"

int main() {
	K test01(constCellsWidth, constCellsWidth, constSizeOfInputVector,
			constNumIterations, constDataSrc, constInitMapRadius,
			constStartLearningRate);
	test01.run();
	return 0;
}
