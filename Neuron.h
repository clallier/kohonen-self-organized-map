/*
 * Neuron.h
 */

#ifndef NEURON_H_
#define NEURON_H_

#include <iostream>
#include <vector>
#include <cmath>
#include "Util.h"

class Neuron {
public:
	Neuron(double x, double y, int numWeights);
	virtual ~Neuron();
	double GetDistance(const std::vector<double> &inputVector) const;
	void AdjustWeights(const std::vector<double> &winnerVector, double learningRate, double influence);
	double X() const;
	double Y() const;
	int Class() const;
	void SetClass( const unsigned int newClass);

private:
	// poids du neurone
	std::vector<double> myWeights;

	// position dans le reseau 2D
	double myX;
	double myY;

	int myClass;

};

#endif /* NEURON_H_ */
