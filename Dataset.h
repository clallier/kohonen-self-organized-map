/*
 * Dataset.h
 */

#ifndef DATASET_H_
#define DATASET_H_

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include "Util.h"

class Dataset {
public:
	Dataset();
	virtual ~Dataset();
	std::vector<std::vector<double> > GetDataSet() const;
	std::vector<int> GetLabels() const;
	void SetDataSet(std::string source);

private:
	std::vector<std::vector<double> > myDataSet;
	std::vector<int> myLabels;
};

#endif /* DATASET_H_ */
