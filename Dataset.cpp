/*
 * Dataset.cpp
 */

#include "Dataset.h"

using namespace std;

// ***************************************************************************
// ***************************************************************************
// ***************************************************************************
Dataset::Dataset() {
}

Dataset::~Dataset() {
}

// ***************************************************************************
// ***************************************************************************
// ***************************************************************************
std::vector<std::vector<double> > Dataset::GetDataSet() const {
	return myDataSet;
}

// ***************************************************************************
// ***************************************************************************
// ***************************************************************************
std::vector<int> Dataset::GetLabels() const {
	return myLabels;
}

// ***************************************************************************
// ***************************************************************************
// ***************************************************************************
void Dataset::SetDataSet(std::string src) {
	ifstream file(src.c_str(), ios::in);

	// traitement du fichier
	// 1 pattern par ligne
	// 1 char (int) = type de pattern [0-9]
	// 256 char (double) suivant = pattern
	// tout les caractères sont separés par un espace.

	if (!file.fail()) {

		string buffer;

		//pour chaque ligne
		while (getline(file, buffer)){

			// utilise un StringStream pour parser le string
			istringstream subBuffer;
			int label;
			double val;
			subBuffer.str(buffer);

			// 1er chiffre (int) : label
			subBuffer >> label;
			myLabels.push_back(label);

			// tous les autres (double)  : vecteur
			vector<double> dVals;
			 while( subBuffer >> val ){
				 dVals.push_back(val);
				 //cout<< val<< " ";
			 }
			 myDataSet.push_back(dVals);
			 //cout<< endl;
		//cout <<buffer <<endl;
		}
		cout << "lecture de la base OK"<< endl;
	} else {
		cerr << "impossible de trouver le fichier" << endl;
	}
}
