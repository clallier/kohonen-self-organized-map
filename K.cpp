/*
 * K.cpp
 */

#include "K.h"

K::K(int cellWidth, int cellHeight, int vectorSize, int cycles,
		std::string dataSetScr, double radius, double learningRate) {
	Util::Init();
	myTestDatas.SetDataSet(dataSetScr);
	myTrainDatas.SetDataSet(dataSetScr);
	mySom = new SOM(cellWidth, cellHeight, vectorSize, cycles, radius, learningRate);

}

K::~K() {
	delete mySom;
}

int K::run() {

	mySom->Train(myTrainDatas.GetDataSet());
	mySom->Test(myTestDatas.GetDataSet(), myTestDatas.GetLabels());
	std::cout << "good job" << std::endl;
	return 0;
}
