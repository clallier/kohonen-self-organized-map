/*
 * define.h
 */

#ifndef DEFINE_H_
#define DEFINE_H_

const int constCellsWidth = 10;
const int constCellsHeight = 10;

//nombre de poids de chaque vecteur de la base
const unsigned int constSizeOfInputVector = 256;

// nombre de boucles pour l'apprentissage
const int constNumIterations = 100000;

//valeur de learning rate au debut de l'entrainement
const double constStartLearningRate = 0.1;

// radius initial
const double constInitMapRadius = 8;

// source de la base
const std::string constDataSrc = "../Datapic_HandDigit07.txt";

#endif /* DEFINE_H_ */
