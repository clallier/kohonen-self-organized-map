/*
 * K.h
 */

#ifndef K_H_
#define K_H_

#include"SOM.h"
#include"define.h"
#include"Util.h"
#include"Dataset.h"
#include<iostream>

class K {
public:
	K(int cellWidth, int cellHeight, int vectorSize, int cycles,
			std::string dataSetScr, double radius, double learningRate);
	virtual ~K();
	int run();

private:
	Dataset myTrainDatas;
	Dataset myTestDatas;
	SOM *mySom;
};

#endif /* K_H_ */
